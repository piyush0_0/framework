#!/usr/bin/env python

import os, sys, argparse


#Edit these values to change the input and output folder with respect to this program
#which was originally in framework/dev
sass_folder = "./assets/sass"
css_folder = "../webroot/assets/css"
css_files = "\"" + css_folder + "/**/*.css\""
work_only_from = "/dev"


def run(cmd):
    print(cmd)
    os.system(cmd)


def main():
    usage_msg = "./sass-cdli.py [OPTIONS]... "
    des_msg="Wrapper for calls to LibSass and Autoprefixer. Converts SCSS to CSS vendor prefixes. Compatible with Bootstrap 4."

    #Parser Options
    parser = argparse.ArgumentParser(usage=usage_msg, description=des_msg)
    parser.add_argument("-n", "--no-watch",
                        action="store_true", dest="no_watch",
                        help="Do not keep watching for changes in the SCSS. Compile and prefix once and then exit.")
    parser.add_argument("-o", "--output-style",
                        action="store", dest="style",
                        help="Default is compressed for production CSS. Nested is best for development CSS and compact is the middleground. (compressed | compact | nested) ")

    args = parser.parse_args()


    # Checking if this is the correct directory to run this program
    pwd = os.getcwd().replace('\\', '/')
    if (not pwd.endswith(work_only_from)):
        print("This script is set to work in", work_only_from)
        print("Either run script from this directory and not", pwd)
        print("or change the global variables of this program.")
        return


    #First checking if NPM exists on the system
    print("Checking NPX Version:")
    no_npx = os.system("npx --version")
    if (no_npx):
        print("Checking NPM version:")
        no_npm = os.system("npm --version")
        if (no_npm):
            print("\n!!! Node Package Manager (npm) is needed. See https://nodejs.org/en/download/ to install it or sudo apt install npm.")
        else:
            print("\n!!! Install NPX globally using npm. See https://www.npmjs.com/package/npx to install it or sudo npm install --global npx.")
        return

    
    node_sass = ['npx --no-install node-sass', sass_folder, '--output', css_folder]
    postcss = ['npx --no-install postcss', css_files, '--replace --no-map --verbose --use autoprefixer']

    # Contructing commands as per the given arguments
    node_sass.append('--output-style')
    if (args.style is not None):
        if (args.style == 'compact'):
            node_sass.append('compact')
        elif (args.style == 'nested'):
            node_sass.append('nested')
        else:
            node_sass.append('compressed')
    else:
        node_sass.append('compressed')

    # Printing what is going to be run
    node_cmd = ' '.join(node_sass)
    postcss_cmd = ' '.join(postcss)

    print('\nRunning once to update all CSS:')
    run(node_cmd)

    if (not args.no_watch):
        node_cmd += ' --watch'
        print('\nWatching for changes, press Ctrl-C to stop watching:')
        try:
            run(node_cmd)
        except KeyboardInterrupt:
            print('\nNot watching anymore.')
    
    print("\nRunning autoprefixer before exiting:")
    run(postcss_cmd)


if __name__ == "__main__":
    sys.exit(main())