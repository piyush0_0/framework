dropArea = document.getElementById("drop-area");
fileElem = document.getElementById("fileElem");
cards = document.getElementById('cards');

filelist = new Map();
duplicatelist = [];
re = /(\d{3})_(R|TE|BE|LE|O|RE)\.jpg|jpeg|tiff|tif|png|bmp|raw/i;


['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
  dropArea.addEventListener(eventName, preventDefaults, false)
  document.body.addEventListener(eventName, preventDefaults, false)
});


['dragenter', 'dragover'].forEach(eventName => {
  dropArea.addEventListener(eventName, highlight, false)
});


['dragleave', 'drop'].forEach(eventName => {
  dropArea.addEventListener(eventName, unhighlight, false)
});

function highlight(e) {
  dropArea.classList.add('highlight');
}

function unhighlight(e) {
  dropArea.classList.remove('active');
}


dropArea.addEventListener('drop', handleDrop, false);

function preventDefaults(e) {
  e.preventDefault();
  e.stopPropagation();
}

function handleDrop(e) {
  var dt = e.dataTransfer;
  files = dt.files;
  e.dataTransfer.clearData();
  handleFiles(files);
}


function handleFiles(filesR) {
  display = [];
  const files = [...filesR];
  files.forEach(function(ele){
    if(filelist.has(ele.name)) duplicatelist.push(ele.name)
    else {
      filelist.set(ele.name, ele);
      display.push(ele.name);
    };
  })
  fileElem.value = "";
  display.forEach(previewFile);
  if(duplicatelist.length){
    console.log(duplicatelist);
    liststr = 'Following files are discarded due to duplicacy\n\n';
    filestr = duplicatelist.join('\n');
    console.log(liststr+filestr);
    alert(liststr+filestr);
    duplicatelist.length = 0;
  }

}

function previewFile(file) {
  regres = file.match(re);
  cross = document.createElement('span');
  cross.classList.add('close');
  cross.innerText = 'x';
  cross.id = file+"span";
  cross.addEventListener("click", function(){
    liElem = this.closest('li');
    this.remove();
    filelist.delete(liElem.innerText);
    if(liElem.parentElement.childElementCount == 1){
      cardElem = liElem.parentElement.closest('div');
      liElem.remove();
      cardElem.remove();
    }
    liElem.remove();
     
  });
  
  element = document.createElement('li');
  element.classList.add('list-group-item');
  element.classList.add('p-1');
  element.innerText = file;
  element.appendChild(cross);
  
  
  if(regres && regres.length == 3 && !regres.includes(undefined)){
    card_exist =  document.getElementById(regres[1]);
    if (card_exist)
    {
      main = document.getElementById(regres[1]+'list');
      main.appendChild(element);
      if(main.childElementCount == 6){
        main.closest('div').classList.remove('bg-warning');
        main.closest('div').classList.add('bg-success');
      }

    } else {
      card = document.createElement('div');
      card.classList.add('card');
      card.classList.add('bg-warning');
      card.id = regres[1];
      header = document.createElement('div');
      header.classList.add('card-header');
      header.classList.add('text-white');
      header.classList.add('p-2');
      header.innerText = regres[1];
      list = document.createElement('ul');
      list.classList.add('list-group');
      list.classList.add('list-group-flush');
      list.id = (regres[1]+'list');

      card.appendChild(header);
      list.appendChild(element);
      card.appendChild(list);

      cards.appendChild(card);
    }
  } else {
    if (document.getElementById('wrongFilename'))
    {
      main = document.getElementById('wrongFilename');
      main.appendChild(element);

    } else {
      card = document.createElement('div');
      card.classList.add('card');
      card.classList.add('bg-danger');
      card.id = 'incorrectcard';
      header = document.createElement('div');
      header.classList.add('card-header');
      header.classList.add('text-white');
      header.classList.add('p-2');
      header.innerText = "Incorrect";
      list = document.createElement('ul');
      list.classList.add('list-group');
      list.classList.add('list-group-flush');
      list.id = ('wrongFilename');

      card.appendChild(header);
      list.appendChild(element);
      card.appendChild(list);

      cards.appendChild(card);
    }
    // console.error(regres);
  }

}

function submitFiles() {
  console.log("Doing this");
  filelist.forEach(uploadFile);
  console.log("Completed");
}

function uploadFile(file){
  console.log(file.name);
}

function uploadFile(file, i) {
  $.ajax({
    type: "post",
    url: "/admin/uploads/geturl",
    data: { 'name': file.name, 'type': file.type, 'mime': file.size},
    dataType: 'json',
    success: function(res){
      // gallery.children[i].children[0].className = "fa fa-spinner";
      $.ajax({
        type: 'PUT',
        url: res.presignedUrl,
        contentType: file.type,
        processData: false,
        data: file,
        success: function(res){
          cross = document.getElementById(file.name+'span')
          cross.click();
        },
        error: function(err){
          console.error("Error uploading the file", err);
          alert("Failed to upload a file.");
        }

      })
  
    },
    error: function(status,error){
      console.log("Cannot upload file contact admin.")
      alert("Failed to initalize upload.")
    },
    complete: function(){
      console.log('complete call');
      
    }

  })

}




