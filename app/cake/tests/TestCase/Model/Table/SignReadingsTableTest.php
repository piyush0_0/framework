<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SignReadingsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SignReadingsTable Test Case
 */
class SignReadingsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\SignReadingsTable
     */
    protected $SignReadings;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.SignReadings',
        'app.Periods',
        'app.Proveniences',
        'app.Languages',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('SignReadings') ? [] : ['className' => SignReadingsTable::class];
        $this->SignReadings = TableRegistry::getTableLocator()->get('SignReadings', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->SignReadings);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
