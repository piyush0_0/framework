<?php
namespace App\View;

use Cake\View\XmlView as CakeXmlView;

class XmlView extends CakeXmlView
{
    use LinkedDataTrait;

    protected $subDir = 'rdf';

    protected $_responseType = 'rdf';

    protected function _serialize($serialize)
    {
        $data = $this->_dataToSerialize($serialize);
        $graph = $this->prepareDataExport($data);
        return $graph->serialise('rdfxml');
    }
}
