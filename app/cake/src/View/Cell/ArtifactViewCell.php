<?php
namespace App\View\Cell;

use Cake\View\Cell;

class ArtifactViewCell extends Cell
{
    public function display($id)
    {
        $this->loadModel('Artifacts');
        $artifact = $this->Artifacts->get($id, [
            'contain' => [
                'Proveniences',
                'Origins',
                'Periods',
                'ArtifactTypes',
                'Archives',
                'Collections',
                'Dates',
                'ExternalResources',
                'Genres',
                'Languages',
                'Materials',
                'MaterialAspects',
                'MaterialColors',
                'Publications',
                'Publications.Authors',
                'Publications.EntryTypes',
                'Publications.Journals',
                'Witnesses',
                'Witnesses.Inscriptions',
                'Impressions',
                'Composites',
                'Seals',
                'ArtifactsShadow',
                'Inscriptions',
                // 'RetiredArtifacts'
            ]
        ]);

        $this->set(compact('artifact'));
    }
}
