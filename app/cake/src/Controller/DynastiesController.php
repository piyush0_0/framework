<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Dynasties Controller
 *
 * @property \App\Model\Table\DynastiesTable $Dynasties
 *
 * @method \App\Model\Entity\Dynasty[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DynastiesController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('LinkedData');

        // Set access for public.
        $this->Auth->allow(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Proveniences', 'Dates', 'Rulers']
        ];
        $dynasties = $this->paginate($this->Dynasties);

        $this->set(compact('dynasties'));
        $this->set('_serialize', 'dynasties');
    }

    /**
     * View method
     *
     * @param string|null $id Dynasty id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $dynasty = $this->Dynasties->get($id, [
            'contain' => ['Proveniences', 'Dates', 'Rulers']
        ]);

        $this->set('dynasty', $dynasty);
        $this->set('_serialize', 'dynasty');
    }
}
