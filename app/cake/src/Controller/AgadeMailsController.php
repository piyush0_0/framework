<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * AgadeMails Controller
 *
 * @property \App\Model\Table\AgadeMailsTable $AgadeMails
 *
 * @method \App\Model\Entity\AgadeMail[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AgadeMailsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize()
    {
        parent::initialize();

        // Set access for public.
        $this->Auth->allow(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $data = $this->request->getData();
        if ($data == []) {
            $filter_tag = null;
        } else {
            $filter_tag = (int) $data['filter_tag'];
        }
        
        $this->paginate = [
            'limit' => 10
        ];

        $cdli_tags = $this->AgadeMails->CdliTags->find('list', ['valueField' => 'cdli_tag','order' => 'cdli_tag'])->all();
        if ($filter_tag != null) {
            $agademails = $this->paginate($this->AgadeMails->find('all', ['contain' => ['CdliTags']])->innerJoinWith('CdliTags')->where(['is_public' => true, 'CdliTags.id' => $filter_tag]));
        } else {
            $agademails = $this->paginate($this->AgadeMails->find('all', ['contain' => ['CdliTags']])->where(['is_public' => true]));
        }
            
        $this->set(compact('agademails', 'cdli_tags', 'filter_tag'));
    }

    /**
     * View method
     *
     * @param string|null $id AgadeMail id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null, $filter_tag = null)
    {
        if ($filter_tag != null) {
            $agademails = $this->paginate($this->AgadeMails->find('all', ['contain' => ['CdliTags']])->innerJoinWith('CdliTags')->where(['is_public' => true, 'CdliTags.id' => $filter_tag]));
        } else {
            $agademails = $this->paginate($this->AgadeMails->find('all', ['contain' => ['CdliTags']])->where(['is_public' => true]));
        }
        $agademail_ids = [];
        foreach ($agademails as $entry) {
            array_push($agademail_ids, $entry['id']);
        }

        $current_pos = array_search($id, $agademail_ids);
        $prev_pos = $current_pos - 1;
        $next_pos = $current_pos + 1;

        $prev_id = null;
        if ($prev_pos >= 0) {
            $prev_id = $agademail_ids[$prev_pos];
        }

        $next_id = null;
        if ($next_pos < count($agademail_ids)) {
            $next_id = $agademail_ids[$next_pos];
        }
        
        $agademail = $this->AgadeMails->find()->where(['id' => $id])->first();
        $this->set(compact('agademail', 'prev_id', 'next_id', 'filter_tag'));
    }
}
