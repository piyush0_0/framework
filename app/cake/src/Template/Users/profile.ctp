<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<main class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <div class="capital-heading"><?= __('User Profile') ?></div>
        <?= $this->Form->create("", ['type'=>'get']) ?>
            <table class="table-bootstrap user-profile-table">
                <tbody>
                    <tr>
                        <th scope="row"><?= __('Username') ?></th>
                        <td><?= h($user['username']) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Email') ?></th>
                        <td><?= h($user['email']) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Created') ?></th>
                        <td><?= h($user['created_at']) ?></td>
                    </tr>
                    <?php if (in_array(1, $user['roles'])) { ?>
                    <tr>
                        <th scope="row"><?= __('Administrator') ?></th>
                        <td><?= __('Yes'); ?></td>
                    </tr>
                    <?php } ?>
                    <?php if (in_array(2, $user['roles'])) { ?>
                    <tr>
                        <th scope="row"><?= __('Editor') ?></th>
                        <td><?= __('Yes'); ?></td>
                    </tr>
                    <?php } ?>
                    <?php if (in_array(3, $user['roles'])) { ?>
                    <tr>
                        <th scope="row"><?= __('Can Download HD Images') ?></th>
                        <td><?= __('Yes'); ?></td>
                    </tr>
                    <?php } ?>
                    <?php if (in_array(4, $user['roles'])) { ?>
                    <tr>
                        <th scope="row"><?= __('Can View Private Artifacts') ?></th>
                        <td><?= __('Yes'); ?></td>
                    </tr>
                    <?php } ?>
                    <?php if (in_array(5, $user['roles'])) { ?>
                    <tr>
                        <th scope="row"><?= __('Can View Private Inscriptions') ?></th>
                        <td><?= __('Yes'); ?></td>
                    </tr>
                    <?php } ?>
                    <?php if (in_array(6, $user['roles'])) { ?>
                    <tr>
                        <th scope="row"><?= __('Can Edit Transliterations') ?></th>
                        <td><?= __('Yes'); ?></td>
                    </tr>
                    <?php } ?>
                    <?php if (in_array(7, $user['roles'])) { ?>
                    <tr>
                        <th scope="row"><?= __('Can View Private Images') ?></th>
                        <td><?= __('Yes'); ?></td>
                    </tr>
                    <?php } ?>
                    <?php if (in_array(8, $user['roles'])) { ?>
                    <tr>
                        <th scope="row"><?= __('Can View CDLI Tablet Manager') ?></th>
                        <td><?= __('Yes'); ?></td>
                    </tr>
                    <?php } ?>
                    <?php if (in_array(9, $user['roles'])) { ?>
                    <tr>
                        <th scope="row"><?= __('Can View CDLI Forums') ?></th>
                        <td><?= __('Yes'); ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        <?= $this->Form->end() ?>

    </div>

        <div class="col-lg boxed">
            <div class="capital-heading"><?= __('Related Actions') ?></div>

            <?= $this->Flash->render() ?>
            <?= $this->Html->link(__('Edit your profile'), ['action' => 'profile', 'edit'], ['class' => 'btn']) ?>
        </div>
</main>
