	<?php if(!empty($result)){?>
		<?php foreach ($result as $row) {
			echo "<h1> Publication Title/".$row["_matchingData"]["Publications"]["bibtexkey"]."</h1>";?>
			<div style="border-style:solid">
				<table >
					<tr>
						<td >
							<?php echo "Period  <br>".$row["_matchingData"]["Periods"]["period"];?>
						</td>
						<td >
							<?php echo "Museum Collections  <br>".$row["_matchingData"]["Collections"]["collection"];?>
						</td>
					</tr>
					<tr>
						<td >
							<?php echo "Provenience  <br>".$row["_matchingData"]["Proveniences"]["provenience"];?>
						</td>
						<td >
							<?php echo "Object Type:  <br>".$row["_matchingData"]["ArtifactTypes"]["artifact_type"];?>
						</td>
					</tr>
					<tr>
						<td >
							<?php echo "Genre/subgenre  <br>".$row["_matchingData"]["Genres"]["genre"];?>
						</td>
						<td >
							<?php echo "Material:  <br>".$row["_matchingData"]["Materials"]["material"];?>
						</td>
					</tr>
					<tr>
						<td >
							<?php echo "Language  <br>".$row["_matchingData"]["Languages"]["language"];?>
						</td>
						<td ></td>
					</tr>
				</table>
			</div>
			<div style="border-style:solid">
				<?php echo "<h2> Translation/Transliteration</h2>";?>
				<table>
					<tr>
						<td>
							<?php echo "Obverse <br>"; ?>
							<?php echo $row["_matchingData"]["Inscriptions"]["transliteration"]; ?>
						</td>
						<td>							
							<?php echo "Reverse <br>"; ?>
							<?php echo $row["_matchingData"]["Inscriptions"]["transliteration_clean"]; ?>
						</td>
					</tr>
				</table>
			</div>
			<div style="border-style:solid">
				<?php echo "<h2> Collection information</h2>";?>
				<table>
					<tr>
						<td>
							<?php echo "Owner: ".$row["_matchingData"]["Collections"]["collection"]; ?>
						</td>
					</tr>
					<tr>
						<td>
							<?php echo "Museum No: ".$row["museum_no"]; ?>
						</td>
					</tr>

					<?php if (!empty($row["accession_no"])) {?>
					<tr>
						<td>
							<?php echo "Accession No: ".$row["accession_no"]; ?>
						</td>
					</tr>
					<?php }?>
					

				</table>
			</div>
			<div style="border-style:solid">
				<?php echo "<h2> Object Data</h2>";?>
				<?php echo "Physical information";?>
				<table>
					<tr>
						<td>
							<?php echo "Object type: ".$row["_matchingData"]["ArtifactTypes"]["artifact_type"]; ?>
						</td>
					</tr>
					<tr>
						<td>
							<?php echo "Material: ".$row["_matchingData"]["Materials"]["material"]; ?>
						</td>
					</tr>
					<tr>
						<td>
							<?php echo "Seal no".$row["seal_no"]; ?>
						</td>
					</tr>
				</table>
				<?php echo "Chronology";?>
				<table>
					<tr>
						<td>
							<?php echo "Period: ".$row["_matchingData"]["Periods"]["period"]; ?>
						</td>
					</tr>

					<?php if (!empty($row["dates_referenced"])) {?>
					<tr>
						<td>
							<?php echo "Dates referenced: ".$row["dates_referenced"]; ?>
						</td>
					</tr>
					<?php }?>

				</table>
				<?php echo "Provenience";?>
				<table>
					<tr>
						<td>
							<?php echo "Provenience: ".$row["_matchingData"]["Proveniences"]["provenience"]; ?>
						</td>
					</tr>
					<tr>
						<td>
							<?php echo "Excavation no.: ".$row["excavation_no"]; ?>
						</td>
					</tr>
				</table>
			</div>
			<div style="border-style:solid">
				<?php echo "<h2> Publication Data</h2>";?>
				<table>
					<tr>
						<td>
							<?php echo "Primary Publication: ".$row["_matchingData"]["Publications"]["collection"]; ?>
						</td>
					</tr>
					<tr>
						<td>
							<?php echo "Author: ".$row["_matchingData"]["Authors"]["author"]; ?>
						</td>
					</tr>
					<tr>
						<td>
							<?php echo "Publication date: ".$row["_matchingData"]["Publications"]["year"];; ?>
						</td>
					</tr>
					<tr>
						<td>
							<?php echo "Secondary publication(s): "; 
							foreach ($secPubs as $secPub) {
							 	print_r($secPub["_matchingData"]["Publications"]["designation"]);
							 } ?>
						</td>
					</tr>
					<tr>
						<td>
							<?php echo "CDLI no: ".$row["id"]; ?>
						</td>
					</tr>
					<tr>
						<td>
							<?php echo "UCLA Library Ark: ".$row["ark_no"]; ?>
						</td>
					</tr>

					<?php if (!empty($row["cdli_comments"])) {?>
					<tr>
						<td>
							<?php echo "CDLI comments: ".$row["cdli_comments"]; ?>
						</td>
					</tr>
					<?php }?>
					
				</table>
			</div>
		<?php }?>
		<?php }?>