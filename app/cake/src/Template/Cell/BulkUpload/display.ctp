    <?php if (isset($error_list)): ?>
        <h3 class="display-4 pt-3 text-left"><?= __('Error Report') ?></h3>

        <div class="table-responsive">
            <table class="table-bootstrap my-3 mx-0">
                <thead>
                <tr>
                    <th scope="col"><?= __('Line No.') ?></th>
                    <th scope="col" colspan=<?= count($header) ?>><?= __('Data') ?></th>
                    <th scope="col" nowrap="nowrap"><?= __('Errors') ?></th>
                </tr>
                <tr>
                    <th scope="col"></th>
                    <?php foreach ($header as $field): ?>
                        <th scope="col"><?= __($field) ?></th> 
                    <?php endforeach ?>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                    <?php foreach ($error_list as $error): ?>
                    <tr>
                        <td> <?= h($error[0]) ?></td>
                        <?php foreach (array_combine($header,explode('","', $error[1])) as $value): ?>
                            <td><?= h($value) ?></td> 
                        <?php endforeach ?>
                        <td nowrap="nowrap">
                            <?php foreach ($error[2] as $row): ?>
                                <?= h($row) ?><br>
                            <?php endforeach; ?>
                            <?php if(isset($error[3])): ?>
                                    <?= $this->Html->link('(Edit Link)', ['controller' => $table, 'action' => 'edit', $error[3]], ['target' => '_blank']) ?>
                            <?php endif ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        
        <?= $this->Form->create(null, ['url' => ['action' => 'export']]) ?>
            <?= $this->Form->input('error_list', ['type' => 'hidden', 'value' => base64_encode(serialize($error_list))]) ?>
            <?= $this->Form->input('header', ['type' => 'hidden', 'value' => base64_encode(serialize($header))]) ?>
            <div class="justify-content-md-center row">
                <table>
                    <tr>
                        <td><?= $this->Form->submit('Export Errors as CSV', ['class' => 'btn cdli-btn-blue']); ?>
                        </td>
                        <td><?= $this->Html->link('Cancel', [
                                'action' => 'add', 'bulk'],
                                ['class' => 'btn cdli-btn-light']) ?> 
                        </td>
                    <tr>
                </table>
            </div>
        <?= $this->Form->end() ?>
    <?php endif; ?>

<div class="mx-0 boxed">
<div class="capital-heading"><?= __('Upload file') ?></div>
    <?= $this->Form->create($table, ['type' => 'file','url' => ['controller'=>$table,'action' => 'add', 'bulk'], 'class'=>'form-inline', 'role'=>'form']) ?>
        <div>
            <label class="sr-only" for="csv"> CSV </label>
            <?php echo $this->Form->input('csv', ['type'=>'file','class' => 'form-control', 'label' => false, 'placeholder' => 'csv upload']); ?>
        </div>
    <?= $this->Form->submit('Submit',['class' => 'btn cdli-btn-blue']) ?>
    <?= $this->Form->end() ?>
</div>


