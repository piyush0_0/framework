<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Inscription $inscription
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Inscriptions'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Credits'), ['controller' => 'Credits', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Credit'), ['controller' => 'Credits', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="inscriptions form large-9 medium-8 columns content">
    <?= $this->Form->create($inscription) ?>
    <fieldset>
        <legend><?= __('Add Inscription') ?></legend>
        <?php
            echo $this->Form->control('artifact_id', ['options' => $artifacts, 'empty' => true]);
            echo $this->Form->control('transliteration');
            echo $this->Form->control('transliteration_clean');
            echo $this->Form->control('tranliteration_sign_names');
            echo $this->Form->control('user_id', ['options' => $users, 'empty' => true]);
            echo $this->Form->control('credit_id', ['options' => $credits, 'empty' => true]);
            echo $this->Form->control('is_latest');
            echo $this->Form->control('annotation');
            echo $this->Form->control('atf2conll_diff_resolved');
            echo $this->Form->control('atf2conll_diff_unresolved');
            echo $this->Form->control('comments');
            echo $this->Form->control('structure');
            echo $this->Form->control('translations');
            echo $this->Form->control('transcriptions');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
