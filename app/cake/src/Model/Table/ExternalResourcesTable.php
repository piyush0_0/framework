<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ExternalResources Model
 *
 * @property \App\Model\Table\ArtifactsTable|\Cake\ORM\Association\BelongsToMany $Artifacts
 *
 * @method \App\Model\Entity\ExternalResource get($primaryKey, $options = [])
 * @method \App\Model\Entity\ExternalResource newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ExternalResource[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ExternalResource|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ExternalResource|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ExternalResource patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ExternalResource[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ExternalResource findOrCreate($search, callable $callback = null, $options = [])
 */
class ExternalResourcesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('external_resources');
        $this->setDisplayField('external_resource');
        $this->setPrimaryKey('id');

        $this->belongsToMany('Artifacts', [
            'foreignKey' => 'external_resource_id',
            'targetForeignKey' => 'artifact_id',
            'joinTable' => 'artifacts_external_resources'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('external_resource')
            ->maxLength('external_resource', 130)
            ->allowEmpty('external_resource');

        $validator
            ->scalar('base_url')
            ->maxLength('base_url', 100)
            ->allowEmpty('base_url');

        $validator
            ->scalar('project_url')
            ->maxLength('project_url', 100)
            ->allowEmpty('project_url');

        $validator
            ->scalar('abbrev')
            ->maxLength('abbrev', 30)
            ->allowEmpty('abbrev');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));

        return $rules;
    }
}
