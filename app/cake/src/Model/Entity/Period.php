<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Period Entity
 *
 * @property int $id
 * @property int|null $sequence
 * @property string $period
 *
 * @property \App\Model\Entity\Artifact[] $artifacts
 * @property \App\Model\Entity\Ruler[] $rulers
 * @property \App\Model\Entity\SignReading[] $sign_readings
 * @property \App\Model\Entity\Year[] $years
 */
class Period extends Entity
{
    use LinkedDataTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'sequence' => true,
        'period' => true,
        'artifacts' => true,
        'rulers' => true,
        'sign_readings' => true,
        'years' => true
    ];

    public function getCidocCrm()
    {
        if ($this->period == 'uncertain') {
            return null;
        }

        preg_match('/(.+) \\(((?:ca\\. )?)(.+?)\\)/', $this->period, $matches);

        $period = [
            '@id' => $this->getUri(),
            '@type' => 'crm:E4_Period',
            'crm:P1_is_identified_by' => [
                'rdfs:label' => $matches[1],
                '@type' => 'crm:E41_Appellation'
            ],
            'crm:P4_has_time-span' => [
                '@type' => 'crm:E52_Time-Span',
                'crm:P78_is_identified_by' => [
                    'rdfs:label' => $matches[3],
                    '@type' => 'crm:E49_Time_Appellation'
                ]
            ],
            'crm:P10i_contains' => !is_array($this->artifacts) ? null : array_map(function ($artifact) {
                return [
                    '@type' => 'crm:E12_Production',
                    'crm:P108_has_produced' => $artifact->getCidocCrm()
                ];
            }, $this->artifacts)
            // TODO rulers
            // TODO sign-readings
            // TODO years
        ];

        if ($matches[2] == 'ca. ') {
            $period['crm:P79_end_is_qualified_by'] =
            $period['crm:P79_beginning_is_qualified_by'] = 'circa';
        }

        return $period;
    }
}
